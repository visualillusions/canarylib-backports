package net.canarymod.hook.player;

import net.canarymod.api.entity.hanging.ItemFrame;
import net.canarymod.api.entity.living.humanoid.Player;
import net.canarymod.api.inventory.Item;
import net.canarymod.hook.CancelableHook;

/**
 * Called when an {@link net.canarymod.api.entity.living.humanoid.Player} sets or removes an {@link net.canarymod.api.inventory.Item} in a {@link net.canarymod.api.entity.hanging.ItemFrame}
 *
 * @author Jason Jones (darkdiplomat)
 */
public final class ItemFrameSetItemHook extends CancelableHook {
    private final Player player;
    private final ItemFrame itemFrame;
    private final Item item;
    private final boolean inserted;

    public ItemFrameSetItemHook(Player player, ItemFrame itemFrame, Item item, boolean inserted) {
        this.player = player;
        this.itemFrame = itemFrame;
        this.item = item;
        this.inserted = inserted;
    }

    /**
     * Gets the {@link net.canarymod.api.entity.living.humanoid.Player} interacting with the {@link net.canarymod.api.entity.hanging.ItemFrame}
     *
     * @return the player
     */
    public final Player getPlayer() {
        return player;
    }

    /**
     * Gets the {@link net.canarymod.api.entity.hanging.ItemFrame} being interacted with
     *
     * @return the item frame
     */
    public ItemFrame getItemFrame() {
        return itemFrame;
    }

    /**
     * Gets the {@link net.canarymod.api.inventory.Item} being placed into or knocked out of the {@link net.canarymod.api.entity.hanging.ItemFrame}
     *
     * @return the item
     */
    public Item getItem() {
        return item;
    }

    /**
     * Gets if an Item is being inserted or removed from the Frame
     *
     * @return {@code true} if inserting an Item; {@code false} if removing an item
     */
    public boolean inserted() {
        return inserted;
    }

    @Override
    public final String toString() {
        return String.format("%s[Player=%s, ItemFrame=%s, Item=%s]", getHookName(), player, itemFrame, item);
    }
}
